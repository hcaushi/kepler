import networkx as nx
import matplotlib.pyplot as plt


def draw_graph(list_people):
    """Display a graph showing relationships between people"""

    graph = nx.MultiGraph()
    graph.add_nodes_from(list_people)

    for person in list_people:
        graph.nodes[person]['name'] = person.name
        graph.add_edges_from([(person, friend) for friend in person.likes], edge_color='blue')
        graph.add_edges_from([(person, enemy) for enemy in person.dislikes], edge_color='red')

    node_labels = nx.get_node_attributes(graph, 'name')
    edge_colors = nx.get_edge_attributes(graph, 'edge_color').values()

    try:
        pos = nx.graphviz_layout(graph)
    except:
        pos = nx.spring_layout(graph, iterations=20)

    nodes = nx.draw_networkx_nodes(graph, pos=pos, node_size=1600, node_color='skyblue', linewidths=2)
    nodes.set_edgecolor('blue')
    edges = nx.draw_networkx_edges(graph, pos=pos, width=3, edge_color=edge_colors)
    nx.draw_networkx_labels(graph, pos=pos, labels=node_labels, font_family='Arial', font_size=16, font_color='black')
    plt.show()
