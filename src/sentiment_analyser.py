import os
import pandas
import random

import spacy
from spacy.util import minibatch, compounding
from nltk.corpus import stopwords

nlp = spacy.load("en_core_web_sm")

# Load the training dataset
df = pandas.read_csv("../datasets/training_data.csv")
training_data = df.iloc[:, [0, 5]]
training_data.iloc[:, 0] = training_data.iloc[:, 0].apply(pandas.to_numeric)
training_data.iloc[:, 0] = (training_data.iloc[:, 0] - 2) * 5
print(training_data)


def train_LSTM(num_iterations=20):
    """Train the LSTM sentiment analyser"""

    textcat = nlp.create_pipe("textcat", config={"architecture": "simple_cnn"})
    textcat.add_label("positive")
    textcat.add_label("negative")
    nlp.add_pipe(textcat, last=True)
    excluded_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]

    with nlp.disabled_pipes(excluded_pipes):
        optimizer = nlp.begin_training()
        batch_sizes = compounding(4., 32., 1.001)
        for i in range(num_iterations):
            loss = {}
            random.shuffle(training_data)
            batches = minibatch(training_data, size=batch_sizes)
            for batch in batches:
                text, labels = zip(*batch)
                nlp.update(text, labels, drop=0.2, sgd=optimizer, losses=loss)


def LSTM(doc):
    """Analyse the sentiment of a provided sentence

    :param doc: A lemmatized nlp(sentence) which has already had its stopwords removed
    :return: A value >0 if the sentiment is positive, or <0 otherwise
    """

    iterations = 3
