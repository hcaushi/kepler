class Person:
    list_people = []

    def __init__(self, name):
        self.name = name
        self.gender = None
        self.likes = []
        self.dislikes = []
        Person.list_people.append(self)

    def add_likes(self, friend):
        self.likes.append(friend)

    def add_dislikes(self, enemy):
        self.dislikes.append(enemy)
