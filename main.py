import sys

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer

import pandas

# from bert_serving.client import BertClient
import spacy
from spacy import displacy
import neuralcoref

import src.drawer
from src.grammar import *
from src.person import Person
import src.sentiment_analyser

# Set up spaCy's pipelines
spacy.prefer_gpu()
nlp = spacy.load("en_core_web_sm")
neuralcoref.add_to_pipe(nlp)
# BERT = spacy.load("en_trf_bertbaseuncased_lg")
stop_words = set(stopwords.words("english"))
ps = PorterStemmer()

dataset = sys.argv[1]
entities = {}
companies = {}

# Load all texts
df = pandas.read_csv(dataset)

"""
for entry in df[df.columns[1]]:
    for sent in nltk.sent_tokenize(entry):
        tokens = [ps.stem(w) for w in word_tokenize(sent) if w not in stop_words]
        tagged = pos_tag(tokens)
        output = grammar.chunker.parse(tagged)
        # output.draw()
"""

# Gather a list of entities
docs = list(nlp.pipe(df[df.columns[1]]))
for doc in docs:
    for entity in doc.ents:
        if entity.label_ == "PERSON":
            entities[entity.text] = entity.label_
        if entity.label_ == "ORG":
            companies[entity.text] = entity.label_

# Print a sample lemmatized sentence
print([token.lemma_ for token in docs[1]])
print(docs[1]._.coref_resolved)

displacy.render(docs[1], style='dep')

# Print named entities
print(entities)
print(companies)
