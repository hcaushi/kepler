# NLP: Kepler

[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=hcaushi_kepler&metric=ncloc)](https://sonarcloud.io/dashboard?id=hcaushi_kepler)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=hcaushi_kepler&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=hcaushi_kepler)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=hcaushi_kepler&metric=coverage)](https://sonarcloud.io/dashboard?id=hcaushi_kepler)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=hcaushi_kepler&metric=bugs)](https://sonarcloud.io/dashboard?id=hcaushi_kepler)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=hcaushi_kepler&metric=sqale_index)](https://sonarcloud.io/dashboard?id=hcaushi_kepler)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=hcaushi_kepler&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=hcaushi_kepler)

**Kepler** is a social graphing tool. When finished, it will be able to analyse text corpora, identify people within the texts, and graph the relationships between people, providing an intuitive way to visualise social interactions within an environment.

Kepler is written in Python using spaCy. It uses BERT to match contexts and sentiments, and to work out the interactions between people. I'm still thinking of adding NLTK functionality to the program, which is dependent on the evolution of the program.

## Usage

This program requires one argument: a two-column CSV file. The second column must contain text corpora; the first column is currently reserved, and will most likely contain a date associated with the corpus (where relevant).
```
+-------------+-------------------------------------------------------------------------------+
| 29-Sep-2020 | I'd just like to interject for a moment. What you're referring to as Linux... |
| 27-Aug-2020 | To be or not to be, that is the question. Whether 'tis nobler in the mind...  |
+-------------+-------------------------------------------------------------------------------+
```

## Requirements

The program requires a number of packages to run. They can be found in `requirements.txt`, but the most important ones are as follows:

* NLTK
* spaCy
* Pandas
* NetworkX

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## The future

To be continued...

## Acknowledgements

The following resources were extremely helpful when writing the program:

* [NLP](https://www.javatpoint.com/nlp)
* [Basics of Natural Language Processing with NLTK](https://ailephant.com/basics-nlp-with-nltk/)
* [Python Data Science Getting Started Tutorial: NLTK](https://medium.com/datadriveninvestor/python-data-science-getting-started-tutorial-nltk-2d8842fedfdd)
* [Named Entity Recognition with NLTK and SpaCy](https://towardsdatascience.com/named-entity-recognition-with-nltk-and-spacy-8c4a7d88e7da)
* [spaCy: Linguistic features](https://spacy.io/usage/linguistic-features)
* [Demistifying BERT](https://www.analyticsvidhya.com/blog/2019/09/demystifying-bert-groundbreaking-nlp-framework/)
* [Stanford's tutorial on tokenization](https://nlp.stanford.edu/IR-book/html/htmledition/tokenization-1.html)

The training dataset is courtesy of [Sentiment140](http://help.sentiment140.com/for-students).